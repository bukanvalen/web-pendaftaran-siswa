<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('landing_page.index');
});

Route::get('/home', 'HomeController@home');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('peserta','PesertaController');

Route::get('/dompdf','PDFController@pdf');

Route::get('/excel', 'PostController@export');

// Route::get('/dompdf', function () {
//     $pdf = App::make('dompdf.wrapper');
//     $pdf->loadHTML('<h1>Test</h1>');
//     return $pdf->stream();
// });