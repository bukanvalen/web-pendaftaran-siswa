<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    //
    protected $table = "users";
    protected $fillable = ["Kursus", "alamat"];
}
