<?php

namespace App\Http\Controllers;
use App\User;
use PDF;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function pdf() {
        $data = User::get();
        $pdf = PDF::loadView('pdf.test', compact('data'));
        return $pdf->download('users.pdf');
    }
}
