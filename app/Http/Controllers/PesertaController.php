<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Peserta;
use Auth;
use Alert;

class PesertaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peserta = User::all(); //::all(); memanggil semua di database
        return view('users.index', compact('peserta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $peserta = User::create([
            "name" => $request["name"],
            "kursus" => $request["kursus"],
            "email" => $request["email"],
            "alamat" => $request["alamat"],
            'password' => Hash::make($request['password']),
            // "profil_id" => Auth::id()
        ]);

        Alert::success('Berhasil', 'Data berhasil disimpan!');

        return redirect('/peserta')->with('success','Data peserta baru berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peserta = User::find($id);
        return view('users.show', compact('peserta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $peserta = User::find($id);
        return view('users.edit', compact('peserta'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update = User::where('id', $id)->update([
            "name" => $request["name"],
            "kursus" => $request["kursus"],
            "email" => $request["email"],
            "alamat" => $request["alamat"],
            // 'password' => Hash::make($request['password']),

            ]);
            
            Alert::success('Berhasil', 'Data berhasil diupdate!');

            return redirect('/peserta')->with('success', 'Data peserta berhasil diupdate!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        Alert::success('Berhasil', 'Data berhasil dihapus!');
        return redirect('/peserta')->with('success', 'Data peserta berhasil dihapus!');
    }
}
