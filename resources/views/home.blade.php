@extends('landing_page.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
            <div class="card mt-5 mb-5">
                <div class="card-header">Logged in!</div>
                    <div class="card-body">
                        <p>Anda berhasil login!</p>                
                        <br><br><a class="btn btn-warning text-decoration-none" role="button" href="{{ url('/') }}">Kembali ke landing page</a>
                        @if(Auth::user()->email == "admin@webq.com" || Auth::user()->role_id == 0)
                        <a class="btn btn-success text-decoration-none" role="button" href="{{ url('/peserta') }}">Masuk ke info peserta</a>
                        @else
                        <a class="btn btn-success text-decoration-none" role="button" href="/peserta/{{Auth::user()->id}}">Lihat profil + materi</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
