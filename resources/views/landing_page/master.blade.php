<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Inovatik">

    <!-- Website Title -->
    <title>WebQ - Kursus Web No. 1</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="{{asset('/landing_page/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('/landing_page/css/swiper.css')}}" rel="stylesheet">
	<link href="{{asset('/landing_page/css/styles.css')}}" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="{{asset('/landing_page/images/favicon.png')}}">
</head>
<body data-spy="scroll" data-target=".fixed-top" class="index">

    @include('landing_page.partials.navbar')

    @yield('content')
    
    @include('landing_page.partials.footer')
    
    <!-- Scripts -->
    <script src="{{asset('/adminlte/plugins/jquery/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="{{asset('/adminlte/plugins/popper/popper.min.js')}}"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="{{asset('/landing_page/js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework -->
    <script src="{{asset('/landing_page/js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{asset('/landing_page/js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders -->
    <script src="{{asset('/landing_page/js/morphext.min.js')}}"></script> <!-- Morphtext rotating text in the header -->
    <script src="{{asset('/landing_page/js/validator.min.js')}}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="{{asset('/landing_page/js/scripts.js')}}"></script> <!-- Custom scripts -->
</body>
</html>