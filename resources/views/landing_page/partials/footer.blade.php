<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text-container">
                    <h3 class="white">Follow WebQ di Sosial Media</h3>
                    <span class="fa-stack">
                        <a href="#">
                            <i class="fab fa-facebook-f fa-stack-1x white"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#">
                            <i class="fab fa-twitter fa-stack-1x white"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#">
                            <i class="fab fa-instagram fa-stack-1x white"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#">
                            <i class="fab fa-linkedin-in fa-stack-1x white"></i>
                        </a>
                    </span>
                </div> <!-- end of text-container -->
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of footer -->  
<!-- end of footer -->


<!-- Copyright -->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="p-small">Copyright © 2021 | SanberCode Laravel Batch 21 Final Project<br><a href="https://inovatik.com">Template by Inovatik</a></p>
            </div> <!-- end of col -->
        </div> <!-- enf of row -->
    </div> <!-- end of container -->
</div> <!-- end of copyright --> 
<!-- end of copyright -->