@extends('adminlte.master')

@section('content')
<div class="container pl-3 pt-3">
<div class=" mt-3 ml-3 mb-3">
    <h4>{{$peserta->name}}</h4>
    <p>ID Peserta: {{$peserta->id}}</p>
    <p>Email: {{$peserta->email}}</p>
    <p>Alamat: {{$peserta->alamat}}</p>
    <p>Program:
    @if ($peserta->kursus == 'fullstack')
    Full-stack Web Development</p><br><br>
    <p>Materi: </p>
    <a href="https://www.youtube.com/watch?v=eRZFGSCkAnw&list=PLFIM0718LjIX-xNEj9bSBuDmPw9FWGMgF" target="_blank">Introduction to Laravel</a><br>
    <a href="https://www.youtube.com/watch?v=sSLJx5t4OJ4&list=PLFIM0718LjIW-XBdVOerYgKegBtD6rSfD" target="_blank">Introduction to Node.js</a>
    @elseif ($peserta->kursus == 'frontend')
    Front-end Web Development</p><br><br>
    <p>Materi: </p>
    <a href="https://www.youtube.com/watch?v=sSLJx5t4OJ4&list=PLFIM0718LjIW-XBdVOerYgKegBtD6rSfD" target="_blank">Introduction to Node.js</a>
    @elseif ($peserta->kursus == 'backend')
    Back-end Web Development</p><br><br>
    <p>Materi: </p>
    <a href="#">Introduction to Laravel</a>
    @else
    NULL</p>
    @endif
</div>
@if(Auth::user()->email == "admin@webq.com" || Auth::user()->role_id == 0)
<a href="/peserta" class="btn btn-warning btn-sm ml-3">Kembali ke list siswa</a>
</div>
@else
<a href="/" class="btn btn-warning btn-sm ml-3">Kembali ke landing page</a>
</div>
@endif
@endsection