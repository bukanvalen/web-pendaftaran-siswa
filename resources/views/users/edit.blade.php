@extends('adminlte.master')

@section('content')
<div class="pl-3 pt-3" >
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Data {{$peserta->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/peserta/{{$peserta->id}}" method="POST">
    @csrf
    @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Nama</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $peserta->name) }}" placeholder="Masukan Nama Siswa">
          @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Email</label>
          <input type="text" class="form-control" id="email" name="email" value="{{ old('email',$peserta->email) }}" placeholder="email">
          @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Alamat</label>
          <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat',$peserta->alamat) }}" placeholder="alamat">
          @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="kursus">Kursus</label>
            <select class="form-control" name="kursus" value="fullstack">
              <option value="frontend" >Front-end Web Development</option>
              <option value="backend">Back-end Web Development</option>
              <option value="fullstack">Full-stack Web Development</option>
            </select>
            @error('kursus')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        {{-- <div class="form-group">
          <label for="isi">Password</label>
          <input type="password" class="form-control" id="password" name="password" value="{{ old('password',$peserta->password) }}" placeholder="password">
          @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div> --}}
      </div>

    
     
    
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-success">Terapkan</button>
        <a href="/peserta" class="btn btn-warning ">Kembali ke list siswa</a>
      </div>
    </form>
  </div>
</div>
@endsection