@extends('adminlte.master')

@section('content')
  <div class="pt-3 pl-3">
@if(Auth::user()->email == "admin@webq.com" || Auth::user()->role_id == 0)
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Siswa WebQ</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @if(session('success'))
              <div class="alert alert-success">
                  {{session('success')}}
              </div>
              @endif
                <a class="btn btn-primary mb-2" href="/peserta/create">Tambah Siswa Baru</a>
              <table class="table table-bordered mt-2">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Pilihan Program</th>
                    <th style="width: 40px">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                @forelse($peserta as $key => $post)
                    <tr>
                        @if ($key == 0)
                          @if (count($peserta) == 1)
                            <tr>
                              <td colspan="4" align="center">Tidak ada data</td>
                            </tr>
                          @endif
                          @continue
                        @endif
                        <td>{{ $key }}</td>
                        <td>{{ $post->name }}</td>
                        <td>
                        @if ($post->kursus == 'fullstack')
                        Full-stack Web Development
                        @elseif ($post->kursus == 'frontend')
                        Front-end Web Development
                        @elseif ($post->kursus == 'backend')
                        Back-end Web Development
                        @endif
                        </td>
                        <td style="display:flex;">
                            <a href="/peserta/{{$post->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/peserta/{{$post->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            <form action="/peserta/{{$post->id}}" method="post">   
                                @csrf    
                                @method('delete')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="4" align="center">Tidak ada data</td>
                </tr>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            
          </div>
        </div>
          
          @else
            <h1>Ngapain akses dashboard admin? Mau jadi hekel?</h1>
        </div>

@endif
@endsection

{{-- @if(session('success'))
  @push('scripts')
  <script>
      Swal.fire({
          title: "Berhasil!",
          text: "Action berhasil dilaksanakan",
          icon: "success",
          confirmButtonText: "Mantappu!",
      });
  </script>
  @endpush
@endif --}}