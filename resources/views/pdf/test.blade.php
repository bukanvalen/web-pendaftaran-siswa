<h1>List Peserta</h1>
<table>
  <thead>
    <tr>
      <th>Nama</th>
      <th>Program yang dipilih</th>
    </tr>
  </thead>
  <tbody>
@foreach($data as $post)
    <tr>
        <td>{{ $post->name }}</td>
        <td>
        @if ($post->kursus == 'fullstack')
        Full-stack Web Development
        @elseif ($post->kursus == 'frontend')
        Front-end Web Development
        @elseif ($post->kursus == 'backend')
        Back-end Web Development
        @endif
        </td>
    </tr>
@endforeach
    </tbody>
</table>