# Web Pendaftaran Siswa (Kursus Online WebQ)
### SanberCode Laravel Batch 21, Final Project
**Anggota Kelompok 19:**
- I Putu Agus Krisma Putra
- Mohammad Dzaky Arditya
- Valentino Harpa

#### Video demo? [Klik di sini!](https://youtu.be/yPhFUTBLW5U)
#### Deploy Heroku? [Klik di sini!](https://desolate-garden-43231.herokuapp.com/) (tapi error :v hehehe)

## Kami menggunakan back-end Laravel (6.*)
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

### About Laravel
Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. 

## Credits and Attributions 

### Template: **[Aria](https://onepagelove.com/aria)** by **[Inovatik](https://inovatik.com/)** and **[AdminLTE](https://github.com/ColorlibHQ/AdminLTE)** by **[Colorlib](https://colorlib.com/wp/cat/bootstrap/)**

**[Photo 1](https://www.pexels.com/photo/green-and-yellow-printed-textile-330771/)** by **[Markus Spiske](https://www.pexels.com/@markusspiske?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** from **[Pexels](https://www.pexels.com)**

**[Photo 2](https://www.pexels.com/photo/coffee-writing-computer-blogging-34600/)** by **[Negative Space](https://www.pexels.com/@negativespace)** from **[Pexels](https://www.pexels.com)**

**[Photo 3](https://www.pexels.com/photo/information-sign-on-shelf-251225/)** by **[Tranmautritam](https://www.pexels.com/@tranmautritam)** from **[Pexels](https://www.pexels.com)**

**[Photo 4](https://www.pexels.com/photo/black-and-gray-laptop-computer-546819/)** by **[Luis Gomes](https://www.pexels.com/@luis-gomes-166706)** from **[Pexels](https://www.pexels.com)**

**[Photo 5](https://www.pexels.com/photo/black-laptop-computer-turned-on-showing-computer-codes-177598/)** by **[Markus Spiske](https://www.pexels.com/@markusspiske?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** from **[Pexels](https://www.pexels.com)**
